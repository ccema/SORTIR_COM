<?php

namespace App\Repository;

use App\Entity\Participant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Participant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Participant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Participant[]    findAll()
 * @method Participant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipantRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Participant::class);
    }

    // /**
    //  * @return Participant[] Returns an array of Participant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    /**
     * Pour trouver un participant par son pseudo ou son mail
     * @return Participant|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    /*
    public function trouveParPseudoOuMail(array $criteria): ?Participant
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.pseudo = :pseudo')
            ->setParameter('pseudo', $criteria['pseudo'])
            ->orWhere('p.mail = :mail')
            ->setParameter('mail', $criteria['mail'])
            //->andWhere('p.motdepasse = :motdepasse')
            //->setParameter('motdepasse', $criteria['motdepasse'])
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * Pour trouver un participant par son pseudo ou son mail
     * @return Participant|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.pseudo = :pseudo OR p.mail = :mail')
            ->setParameter('pseudo', $username)
            ->setParameter('mail', $username)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
