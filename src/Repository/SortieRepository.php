<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sortie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sortie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sortie[]    findAll()
 * @method Sortie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SortieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sortie::class);
    }

    /**
     * requête retirant les sorties expiré de plus de 1 mois par rapport
     * à la date actuelle
     * @return Sortie[]
     */

    public function sortieConsultable(): array
    {

        return $this->createQueryBuilder('sortie')
            ->andWhere('sortie.dateHeureDebut > :date')
            ->setParameter('date', new \DateTime('-30 day'))
            ->getQuery()
            ->getResult();
    }


    // /**
    //  * @return Sortie[] Returns an array of Sortie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sortie
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * Récupère les sorties en lien avec une recherche
     * @return Sortie[]
     */
    public function findSearch(SearchData $search, int $id): array
    {
        $query = $this
            ->createQueryBuilder('sorties')
            ->select('site', 'sorties')
            ->leftJoin('sorties.site', 'site');

        //Recherche par mot clé
        if (!empty($search->saisie)) {
            $query = $query
                ->andWhere('sorties.nom LIKE :saisie')
                // ->setParameter('saisie', "{%$search->saisie%}");
                ->setParameter('saisie', "%{$search->saisie}%");
        }

        //Recherche par date
        if (!empty($search->dateEntree) && !empty($search->dateFin)) {
            $query = $query
                ->andWhere('sorties.dateHeureDebut BETWEEN :from AND :to')
                ->setParameter('from', $search->dateEntree)
                ->setParameter('to', $search->dateFin);
        }


        //Recherche par campus
        if (!empty($search->sites)) {
            $query = $query
                ->andWhere('site = :sites')
                ->setParameter('sites', $search->sites->getId());

        }

        // Recherche par organisateur
        if (!empty($search->organisateur)) {
            $query = $query
                ->andWhere('sorties.organisateur = :organisateur')
                ->setParameter('organisateur', $id);

        }

        // Recherche par inscrit OU non inscrit
        if (!empty($search->inscrit) || !empty($search->nonInscrit)) {
            $query = $query
                ->leftJoin('sorties.inscriptions', 'inscriptions')
            ;

            //Recherche par inscrit
            if (!empty($search->inscrit)) {
                $query = $query
                    ->andWhere('inscriptions.participant = :inscrit')
                    ->setParameter('inscrit', $id)
                ;
            }

            //Recherche par non inscrit
            if (!empty($search->nonInscrit)) {
                $query = $query
                    ->andWhere('inscriptions.participant != :inscrit OR inscriptions.id IS NULL')
                    ->setParameter('inscrit', $id)
                ;
            }
        }



        // Recherche des sorties passées
        if (!empty($search->sortiesPassees)) {
            $query = $query
                ->Join('sorties.etat', 'sortiesPassees')
                ->andWhere('sortiesPassees.libelle = :sortiesPassees ')
                ->setParameter('sortiesPassees', 'Passée');
        }
        // SELECT * from sortie LEFT JOIN etat ON sortie.etat_id = etat.id WHERE etat.libelle = "Passée"
        return $query->getQuery()->getResult();
    }
}
