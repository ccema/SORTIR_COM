<?php

namespace App\DataFixtures;

use App\Entity\Etat;
use App\Entity\Lieu;
use App\Entity\Participant;
use App\Entity\Site;
use App\Entity\Sortie;
use App\Entity\Ville;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

use Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TestFixtures extends Fixture implements ContainerAwareInterface,FixtureInterface,OrderedFixtureInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // On configure dans quelles langues nous voulons nos données
        $faker = Faker\Factory::create('fr_FR');

        // Création des sites
        $siteNantes = new Site();
        $siteNiort = new Site();
        $siteRennes = new Site();
        $siteNantes->setNom('Nantes');
        $siteNiort->setNom('Niort');
        $siteRennes->setNom('Rennes');
        $manager->persist($siteNantes);
        $manager->persist($siteNiort);
        $manager->persist($siteRennes);
        $manager->flush();

        // Création des participants
        $aurelien = new Participant();
        $aurelien
            ->setActif(true)
            ->setAdministrateur(true)
            ->setMail('aurelien.bregeon2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($aurelien, 'Aurélien'))
            ->setNom('BREGEON')
            ->setPrenom('Aurélien')
            ->setPseudo('Aurélien')
            ->setSite($siteNantes)
            ->setTelephone('0601020304')
        ;

        $cedric = new Participant();
        $cedric
            ->setActif(true)
            ->setAdministrateur(true)
            ->setMail('cedric.stephan2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($cedric, 'Cédric'))
            ->setNom('STEPHAN')
            ->setPrenom('Cédric')
            ->setPseudo('Cédric')
            ->setSite($siteNiort)
            ->setTelephone('0601020304')
        ;

        $claireEurielle = new Participant();
        $claireEurielle
            ->setActif(true)
            ->setAdministrateur(true)
            ->setMail('claireeurielle.bouczo2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($claireEurielle, 'amaguiz'))
            ->setNom('BOUCZO')
            ->setPrenom('Claire-Eurielle')
            ->setPseudo('Lainea')
            ->setSite($siteRennes)
            ->setTelephone('0601020304')
        ;

        $martial = new Participant();
        $martial
            ->setActif(true)
            ->setAdministrateur(true)
            ->setMail('martial.bourgeois2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($martial, 'Martial'))
            ->setNom('BOURGEOIS')
            ->setPrenom('Martial')
            ->setPseudo('Martial')
            ->setSite($siteNantes)
            ->setTelephone('0601020304')
        ;

        $jean = new Participant();
        $jean
            ->setActif(true)
            ->setAdministrateur(false)
            ->setMail('jean.dupond2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($jean, 'Jean'))
            ->setNom('DUPOND')
            ->setPrenom('Jean')
            ->setPseudo('Jean')
            ->setSite($siteNantes)
            ->setTelephone('0102030405')
        ;

        $anne = new Participant();
        $anne
            ->setActif(true)
            ->setAdministrateur(false)
            ->setMail('anne.blaise2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($anne, 'Anne'))
            ->setNom('BLAISE')
            ->setPrenom('Anne')
            ->setPseudo('Anne')
            ->setSite($siteNiort)
            ->setTelephone('0240726605')
        ;

        $amelie = new Participant();
        $amelie
            ->setActif(true)
            ->setAdministrateur(false)
            ->setMail('amelie.dufour2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($amelie, 'Amelie'))
            ->setNom('DUFOUR')
            ->setPrenom('Amelie')
            ->setPseudo('Amelie')
            ->setSite($siteRennes)
            ->setTelephone('0240786918')
        ;

        $alexandre = new Participant();
        $alexandre
            ->setActif(true)
            ->setAdministrateur(false)
            ->setMail('alexandre.legrand2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($alexandre, 'Alexandre'))
            ->setNom('LEGRAND')
            ->setPrenom('Alexandre')
            ->setPseudo('Alexandre')
            ->setSite($siteNantes)
            ->setTelephone('0240892968')
        ;

        $louis = new Participant();
        $louis
            ->setActif(true)
            ->setAdministrateur(false)
            ->setMail('louis.quatorze2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($louis, 'Louis'))
            ->setNom('QUATORZE')
            ->setPrenom('Louis')
            ->setPseudo('Louis')
            ->setSite($siteRennes)
            ->setTelephone('0240193948')
        ;

        $ophelie = new Participant();
        $ophelie
            ->setActif(true)
            ->setAdministrateur(false)
            ->setMail('ophelie.winter2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($ophelie, 'Ophelie'))
            ->setNom('WINTER')
            ->setPrenom('Ophelie')
            ->setPseudo('Ophelie')
            ->setSite($siteRennes)
            ->setTelephone('0240193948')
        ;

        $maud = new Participant();
        $maud
            ->setActif(true)
            ->setAdministrateur(false)
            ->setMail('maud.passe2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($maud, 'Maud'))
            ->setNom('PASSE')
            ->setPrenom('Maud')
            ->setPseudo('Maud')
            ->setSite($siteNiort)
            ->setTelephone('0240398912')
        ;

        $pierre = new Participant();
        $pierre
            ->setActif(true)
            ->setAdministrateur(false)
            ->setMail('pierre.mortimer2020@campus-eni.fr')
            ->setMotdepasse($this->passwordEncoder->encodePassword($pierre, 'Pierre'))
            ->setNom('MORTIMER')
            ->setPrenom('Pierre')
            ->setPseudo('Pierre')
            ->setSite($siteNiort)
            ->setTelephone('0240885511')
        ;

        $manager->persist($aurelien);
        $manager->persist($cedric);
        $manager->persist($claireEurielle);
        $manager->persist($martial);
        $manager->persist($jean);
        $manager->persist($anne);
        $manager->persist($amelie);
        $manager->persist($alexandre);
        $manager->persist($louis);
        $manager->persist($ophelie);
        $manager->persist($maud);
        $manager->persist($pierre);
        $manager->flush();

        // Création des villes
        $villeNantes1 = new Ville();
        $villeNantes1
            ->setNom('Brains')
            ->setCodePostal('44830')
        ;
        $villeNantes2 = new Ville();
        $villeNantes2
            ->setNom('Sautron')
            ->setCodePostal('44194')
        ;
        $villeRennes1 = new Ville();
        $villeRennes1
            ->setNom('Mordelles')
            ->setCodePostal('35196')
        ;
        $villeRennes2 = new Ville();
        $villeRennes2
            ->setNom('Corps-Nuds')
            ->setCodePostal('35088')
        ;
        $villeNiort1 = new Ville();
        $villeNiort1
            ->setNom('Juscorps')
            ->setCodePostal('79144')
        ;
        $villeNiort2 = new Ville();
        $villeNiort2
            ->setNom('Coulon')
            ->setCodePostal('79510')
        ;

        $manager->persist($villeNantes1);
        $manager->persist($villeNantes2);
        $manager->persist($villeNiort1);
        $manager->persist($villeNiort2);
        $manager->persist($villeRennes1);
        $manager->persist($villeRennes2);
        $manager->flush();

        // création de lieux
        $lieu1Nantes = new Lieu();
        $lieu1Nantes
            ->setNom('Billard')
            ->setRue('rue du billard de Brains')
            ->setLatitude('0.123456')
            ->setLongitude('5.789012')
            ->setVille($villeNantes1)
        ;
        $manager->persist($lieu1Nantes);

        $lieu2Nantes = new Lieu();
        $lieu2Nantes
            ->setNom('Bowling')
            ->setRue('allée du Bowling de Sautron')
            ->setLatitude('10.345678')
            ->setLongitude('15.901234')
            ->setVille($villeNantes2)
        ;
        $manager->persist($lieu2Nantes);

        $lieu3Nantes = new Lieu();
        $lieu3Nantes
            ->setNom('Escalade')
            ->setRue('rue de l\'escalade de Brains')
            ->setLatitude('20.567890')
            ->setLongitude('25.123456')
            ->setVille($villeNantes1)
        ;
        $manager->persist($lieu3Nantes);

        $lieu4Nantes = new Lieu();
        $lieu4Nantes
            ->setNom('Cinéma')
            ->setRue('allée du cinéma de Sautron')
            ->setLatitude('30.789012')
            ->setLongitude('35.345678')
            ->setVille($villeNantes2)
        ;
        $manager->persist($lieu4Nantes);

        $lieu1Rennes = new Lieu();
        $lieu1Rennes
            ->setNom('Billard')
            ->setRue('rue du billard de Mordelles')
            ->setLatitude('40.901234')
            ->setLongitude('45.567890')
            ->setVille($villeRennes1)
        ;
        $manager->persist($lieu1Rennes);

        $lieu2Rennes = new Lieu();
        $lieu2Rennes
            ->setNom('Bowling')
            ->setRue('allée du bowling de Corps-Nuds')
            ->setLatitude('50.123456')
            ->setLongitude('55.789012')
            ->setVille($villeRennes2)
        ;
        $manager->persist($lieu2Rennes);

        $lieu3Rennes = new Lieu();
        $lieu3Rennes
            ->setNom('Escalade')
            ->setRue('rue de l\'escalade de Mordelles')
            ->setLatitude('60.345678')
            ->setLongitude('65.901234')
            ->setVille($villeRennes1)
        ;
        $manager->persist($lieu3Rennes);

        $lieu4Rennes = new Lieu();
        $lieu4Rennes
            ->setNom('Cinéma')
            ->setRue('allée du cinéma de Corps-Nuds')
            ->setLatitude('70.567890')
            ->setLongitude('75.123456')
            ->setVille($villeRennes2)
        ;
        $manager->persist($lieu4Rennes);

        $lieu1Niort = new Lieu();
        $lieu1Niort
            ->setNom('Billard')
            ->setRue('rue du billard de Juscorps')
            ->setLatitude('80.789012')
            ->setLongitude('85.345678')
            ->setVille($villeNiort1)
        ;
        $manager->persist($lieu1Niort);

        $lieu2Niort = new Lieu();
        $lieu2Niort
            ->setNom('Bowling')
            ->setRue('allée du bowling de Coulon')
            ->setLatitude('90.901234')
            ->setLongitude('95.567890')
            ->setVille($villeNiort2)
        ;
        $manager->persist($lieu2Niort);

        $lieu3Niort = new Lieu();
        $lieu3Niort
            ->setNom('Escalade')
            ->setRue('rue de l\'escalade de Juscorps')
            ->setLatitude('00.123456')
            ->setLongitude('05.789012')
            ->setVille($villeNiort1)
        ;
        $manager->persist($lieu3Niort);

        $lieu4Niort = new Lieu();
        $lieu4Niort
            ->setNom('Cinéma')
            ->setRue('allée du cinéma de Coulon')
            ->setLatitude('10.345678')
            ->setLongitude('15.901234')
            ->setVille($villeNiort2)
        ;
        $manager->persist($lieu4Niort);

        $manager->flush();

        // Création des états

        $etatCreee = new Etat();
        $etatPubliee = new Etat();
        $etatCloturee = new Etat();
        $etatEnCours = new Etat();
        $etatPassee = new Etat();
        $etatAnnulee = new Etat();

        $etatCreee->setlibelle('Créée');
        $etatPubliee->setlibelle('Publiée');
        $etatCloturee->setlibelle('Cloturée');
        $etatEnCours->setlibelle('En cours');
        $etatPassee->setlibelle('Passée');
        $etatAnnulee->setlibelle('Annulée');

        $manager->persist($etatCreee);
        $manager->persist($etatPubliee);
        $manager->persist($etatCloturee);
        $manager->persist($etatEnCours);
        $manager->persist($etatPassee);
        $manager->persist($etatAnnulee);
        $manager->flush();

        // création de sorties

        $sortie1 = new Sortie();
        $sortie1
            ->setNom('Escalade')
            ->setDateHeureDebut($faker->dateTimeBetween('+16 day','+1 month'))
            ->setDateLimiteInscription($faker->dateTimeBetween('+10 day',$sortie1->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,24))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu1Nantes)
            ->setEtat($etatCreee)
            ->setSite($aurelien->getSite())
            ->setOrganisateur($aurelien)
        ;
        $manager->persist($sortie1);

        $sortie2 = new Sortie();
        $sortie2
            ->setNom('Canoë')
            ->setDateHeureDebut($faker->dateTimeBetween('+10 day','+1 month'))
            ->setDateLimiteInscription($faker->dateTimeBetween('+4 day',$sortie2->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,24))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu2Nantes)
            ->setEtat($etatPubliee)
            ->setSite($aurelien->getSite())
            ->setOrganisateur($aurelien)
        ;
        $manager->persist($sortie2);

        $sortie3 = new Sortie();
        $sortie3
            ->setNom('Tournoi de palets')
            ->setDateHeureDebut($faker->dateTimeBetween('now','+2 month'))
            ->setDateLimiteInscription(new \DateTime('-1 day'))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,10))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu1Niort)
            ->setEtat($etatCloturee)
            ->setSite($cedric->getSite())
            ->setOrganisateur($cedric)
        ;
        $manager->persist($sortie3);

        $sortie4 = new Sortie();
        $sortie4
            ->setNom('Jet ski')
            ->setDateHeureDebut(new \DateTime('now'))
            ->setDateLimiteInscription($faker->dateTimeBetween('-1 month',$sortie4->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,6))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu2Niort)
            ->setEtat($etatEnCours)
            ->setSite($cedric->getSite())
            ->setOrganisateur($cedric)
        ;
        $manager->persist($sortie4);

        $sortie5 = new Sortie();
        $sortie5
            ->setNom('VTT')
            ->setDateHeureDebut($faker->dateTimeBetween('-1 month','-1 day'))
            ->setDateLimiteInscription($faker->dateTimeBetween('-1 month',$sortie5->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,24))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu1Rennes)
            ->setEtat($etatPassee)
            ->setSite($claireEurielle->getSite())
            ->setOrganisateur($claireEurielle)
        ;
        $manager->persist($sortie5);

        $sortie6 = new Sortie();
        $sortie6
            ->setNom('Randonnée')
            ->setDateHeureDebut($faker->dateTimeBetween('+7 day','+1 month'))
            ->setDateLimiteInscription($faker->dateTimeBetween('now',$sortie6->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,24))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu2Rennes)
            ->setEtat($etatAnnulee)
            ->setSite($claireEurielle->getSite())
            ->setOrganisateur($claireEurielle)
        ;
        $manager->persist($sortie6);

        $sortie7 = new Sortie();
        $sortie7
            ->setNom('Football')
            ->setDateHeureDebut($faker->dateTimeBetween('+16 day','+1 month'))
            ->setDateLimiteInscription($faker->dateTimeBetween('+10 day',$sortie7->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,8))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu1Nantes)
            ->setEtat($etatCreee)
            ->setSite($martial->getSite())
            ->setOrganisateur($martial)
        ;
        $manager->persist($sortie7);

        $sortie8 = new Sortie();
        $sortie8
            ->setNom('Basketball')
            ->setDateHeureDebut($faker->dateTimeBetween('+16 day','+2 month'))
            ->setDateLimiteInscription($faker->dateTimeBetween('+8 day',$sortie8->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,4))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu2Nantes)
            ->setEtat($etatPubliee)
            ->setSite($martial->getSite())
            ->setOrganisateur($martial)
        ;
        $manager->persist($sortie8);

        $sortie9 = new Sortie();
        $sortie9
            ->setNom('Equitation')
            ->setDateHeureDebut($faker->dateTimeBetween('+10 day','+3 month'))
            ->setDateLimiteInscription($faker->dateTimeBetween('-10 day',$sortie9->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,4))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu1Niort)
            ->setEtat($etatCloturee)
            ->setSite($alexandre->getSite())
            ->setOrganisateur($alexandre)
        ;
        $manager->persist($sortie9);

        $sortie10 = new Sortie();
        $sortie10
            ->setNom('Natation')
            ->setDateHeureDebut(new \DateTime('now'))
            ->setDateLimiteInscription($faker->dateTimeBetween('-1 month',$sortie10->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,6))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu2Niort)
            ->setEtat($etatEnCours)
            ->setSite($louis->getSite())
            ->setOrganisateur($louis)
        ;
        $manager->persist($sortie10);

        $sortie11 = new Sortie();
        $sortie11
            ->setNom('Surf')
            ->setDateHeureDebut($faker->dateTimeBetween('-10 day','now'))
            ->setDateLimiteInscription($faker->dateTimeBetween('-1 month',$sortie11->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,24))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu1Rennes)
            ->setEtat($etatPassee)
            ->setSite($amelie->getSite())
            ->setOrganisateur($amelie)
        ;
        $manager->persist($sortie11);

        $sortie12 = new Sortie();
        $sortie12
            ->setNom('Découverte des vins')
            ->setDateHeureDebut($faker->dateTimeBetween('+15 day','+1 month'))
            ->setDateLimiteInscription($faker->dateTimeBetween('+1 day',$sortie12->getDateHeureDebut()))
            ->setDuree($faker->dateTime)
            ->setNbInscriptionsMax($faker->numberBetween(1,16))
            ->setInfosSortie($faker->text($maxNbChars = 200))
            ->setLieu($lieu2Rennes)
            ->setEtat($etatAnnulee)
            ->setSite($anne->getSite())
            ->setOrganisateur($anne)
        ;
        $manager->persist($sortie12);

        $manager->flush();

    }

    public function getOrder()
    {
        return 1;
    }
}

