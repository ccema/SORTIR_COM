<?php

namespace App\Form;

use App\Entity\Participant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdatePasswordType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('ancienmotdepasse', PasswordType::class, [
                'label'=>'Mot de passe actuel :',

                'required'=> true,
                'attr' => array(
                    'placeholder'=> 'Mot de passe'
                ),
                'always_empty' => true,
            ])
            ->add('motdepasse', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Nouveau mot de passe :'],
                'second_options' => ['label' => 'Confirmer nouveau mot de passe :'],
                'invalid_message' => 'Les deux mots de passe doivent être identiques',
                'options' => array(
                    'attr' => array(
                        'placeholder' => 'Nouveau mot de passe'
                    )
                ),
                'required' => true,
            ))

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            //mettre le nouveau formulaire
            'data_class' => Participant::class,
            'csrf_token_id' => 'change_password',
        ));
    }

}
