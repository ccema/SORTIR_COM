<?php

namespace App\Form;

use App\Entity\Participant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pseudo',TextType::class,[
                'label'=> 'Pseudo :',
                'label_attr'=>array(
                    'class'=> 'labelform',
                ),
                'attr' => array(
                    'class'=> 'dataform',
                ),
            ])
            ->add('mail', EmailType::class,[
                'label'=> 'Mail :',
                'label_attr'=>array(
                    'class'=> 'labelform',
                ),
                'attr' => array(
                    'class'=> 'dataform',
                ),
            ])
            ->add('telephone', TextType::class,[
                'label'=> 'Téléphone :',
                'label_attr'=>array(
                    'class'=> 'labelform',
                ),
                'attr' => array(
                    'class'=> 'dataform',
                ),
            ])
            ->add('nom', TextType::class,[
                'label'=> 'Nom :',
                'label_attr'=>array(
                    'class'=> 'labelform',
                ),
                'attr' => array(
                    'class'=> 'dataform',
                ),
            ])
            ->add('prenom', TextType::class,[
                'label'=> 'Prénom :',
                'label_attr'=>array(
                    'class'=> 'labelform',
                ),
                'attr' => array(
                    'class'=> 'dataform',
                ),
            ])
            //->add('photo')
            ->add('motdepasse', PasswordType::class,[
                'label'=> 'Confirmer modification :',
                'label_attr'=>array(
                    'class'=> 'labelform',
                ),
                'attr' => array(
                    'placeholder' => 'Votre mot de passe',
                    'class'=> 'dataform',
                ),
                'required' => true,
                'always_empty' => true,

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participant::class,
        ]);
    }
}
