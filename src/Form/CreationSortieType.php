<?php

namespace App\Form;

use App\Entity\Sortie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreationSortieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $nomSite = $options['nomSite'];
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom de la sortie : '
            ])
            ->add('dateHeureDebut', DateTimeType::class, [
                'label' => 'Date et heure de la sortie :',

            ])
            ->add('duree', TimeType::class, [
                'label' => 'Durée :'
            ])
            ->add('dateLimiteInscription', DateType::class, [
                'label' => 'Date limite d\'inscription : '
            ])
            ->add('nbInscriptionsMax', IntegerType::class, [
                'label' => 'Nombre de places : ',
                'attr' => [
                'min' => 1
                ]
            ])
            ->add('infosSortie', TextareaType::class, [
                'label' => 'Description et infos : ',
                'attr' => ['rows' => 4],
            ])
            ->add('site', TextType::class, [
                'disabled' => true,
                'label' => 'Ville organisatrice',
                'attr' => [
                    'value' => $nomSite
                ]
            ])
            ->add('ville', EntityType::class, [
                'label' => 'Ville : ',
                'class' => 'App\Entity\Ville',
                'choice_label' => 'nom',
                'choice_value'=>'id',
                'attr' => [
                    'data-source' => './listesDeroulantes/$id',
                ],
                'mapped' => false
            ])
            ->add('lieu', EntityType::class, [
                'label' => 'Lieu : ',
                'class' => 'App\Entity\Lieu',
                'choice_label' => 'nom',
                'attr' => [
                    'data-source' => './listeDeroulanteLieu/$id',
                ],
                //'attr'=>['style'=>'display : none']
            ])
            ->add('lieuForm', CreationLieuType::class, [
                'mapped' => false
            ])
            ->add('enregistrer', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr'=>[
                    'class'=>'retour',
                ],
            ])
            ->add('publier', SubmitType::class, [
                'label' => 'Publier la sortie',
                'attr'=>[
                    'class'=>'retour',
                ],
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,
        ]);
        $resolver->setRequired([
            'nomSite',
        ]);
    }
}
