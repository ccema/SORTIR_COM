<?php

namespace App\Form;

use App\Entity\Site;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SortieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*champs du formulaire, a travailler*/

            ->add('sites',EntityType::class, [
                'label'=> 'nom',
                'required'=> false,
                'class'=> Site::class,
                'expanded'=> true,
                'multiple'=> true,
            ])

            ->add('nom', SearchType::class, ['label' => 'Le nom de la sortie contient : '])
            ->add('dateHeureDebut', DateType::class, ['label' => 'Entre '])
            ->add('dateLimiteInscription', DateType::class, ['label' => 'et  '])

            ->add('duree', CheckboxType::class, [
                'label'    => 'Sorties dont je suis l\'organisteur/trice',
                'required' => false,
            ])
             ->add('nbInscriptionsMax', CheckboxType::class, [
                'label'    => 'Sorties auxquelles je suis inscrit/e',
                'required' => false,
             ])
             ->add('infosSortie', CheckboxType::class, [
                'label'    => 'Sorties auxquelles je ne suis pas inscrit/e',
                'required' => false,
            ])
             ->add('lieu', CheckboxType::class, [
                'label'    => 'Sorites passées',
                'required' => false,
            ])

            ->add('Rechercher',SubmitType::class)
        ;


            /*afficher le tableau des sorties selon les filtres activer ou pas*/


        /*afficher les sorties sans filtres*/


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Site::class,
            'method'=> 'GET',
            'csrf_protection'=> false
        ]);
    }
}
