<?php


namespace App\Form;


use App\Data\SearchData;
use App\Entity\Site;
use phpDocumentor\Reflection\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{

    //fonction pour construire le formulaire de filtres
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //construction du formulaire de recherche
        $builder

            ->add('sites',EntityType::class,[
                'label' => 'Site:',
                'required' => false,
                'class'=>Site::class,

                 ])

            ->add('saisie', TextType::class, [
                'label' => 'Le nom de la sortie contient:',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Sortie qui contient ...']
            ])
            ->add('dateEntree',DateType::class,[
                'label' => 'Entre',
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('dateFin',DateType::class,[
                'label' => 'et',
                'required' => false,
                'widget' => 'single_text'
            ])





               ->add('organisateur',CheckboxType::class,[
              'label' => 'Sortie dont je suis organisateur/trice',
              'required' => false ])

        ->add('inscrit',CheckboxType::class,[
        'label' => 'Sorties auxquelles je suis inscrit/e',
        'required' => false ])

        ->add('nonInscrit',CheckboxType::class,[
        'label' => 'Sorties auxquelles je ne suis pas inscrit/e',
        'required' => false ])

        ->add('sortiesPassees',CheckboxType::class,[
        'label' => 'Sorties passées',
        'required' => false ]);


    }

    //configuration des options liées au formulaire
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class'=> SearchData::class,
            'csrf_protection' => false
        ]);
    }

    //reconfigurer l'URL
    public function getBlockPrefix() {

        return '';
    }
}