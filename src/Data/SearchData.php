<?php
namespace App\Data;

use App\Entity\Site;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints\Date;

class SearchData
{

    /**
     * @var Site
     */
    public $sites=null;

    /**
     * @var string
     */
    public $nomSortie='';

    /**
     * @var DateType
     */
    public $dateEntree=null;

    /**
     * @var DateType
     */
    public $dateFin=null;

    /**
     * @var Boolean
     */
    public $organisateur=false;

    /**
     * @var Boolean
     */
    public $inscrit=false;

    /**
     * @var string
     */
    public $saisie = '';

    /**
     * @var Boolean
     */
    public $nonInscrit=false;

    /**
     * @var Boolean
     */
    public $sortiesPassees=false;


}