<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Etat;
use App\Entity\Lieu;
use App\Entity\Sortie;
use App\Form\CreationSortieType;
use App\Form\MotifAnnulationType;
use App\Form\SearchType;
use App\Form\SortieType;
use App\Repository\EtatRepository;
use App\Repository\LieuRepository;
use App\Repository\SiteRepository;
use App\Repository\SortieRepository;
use App\Repository\VilleRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SortieController extends AbstractController
{
    /**
     * @Route("/", name="sortie_home")
     */
    public function home(SortieRepository $sortieRepository, Request $request){

        $data=new SearchData();
        $form=$this->createForm(SearchType::class,$data);
        $form->handleRequest($request);
        if ($form->isSubmitted()){

            // second paramètre permet de fournir l'ID de l'utilisateur de la session
        $sorties=$sortieRepository->findSearch($data, $this->getUser()->getId());

       }
        else{
            $sorties=$sortieRepository->sortieConsultable();
        }
        return $this->render('sorties/home.html.twig', [
            'sorties' => $sorties,'form'=>$form->createView()
        ]);
    }

    /**
     * @Route ("/creationSortie", name="creerSortie")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param EtatRepository $etatRepository
     * @return RedirectResponse|Response
     */
    public function creerSortie(Request $request, EntityManagerInterface $manager, EtatRepository $etatRepository) {
        $sortie = new Sortie();
        $organisateur = $this->getUser();
        // création de l'instance du formulaire
        $creationSortieForm = $this->createForm(CreationSortieType::class, $sortie, [
            'nomSite' => $organisateur->getSite()->getNom(),
        ]);
        // association de l'instance de CreationSortieType avec le Request
        $creationSortieForm->handleRequest($request);
        //si le formulaire  a été soumis et qu'il est valide
        if ($creationSortieForm->isSubmitted() && $creationSortieForm->isValid()){
            $sortie->setOrganisateur($organisateur);
            $sortie->setSite($organisateur->getSite());
            $dateJour = new DateTime();

            //conditions pour eviter des incohérences métier (restrictions et affiche message flash)
            if ($sortie->getDateHeureDebut()->format('d m Y') <= $dateJour->format('d m Y')) {
                $this->addFlash('warning',
                    'La date et l\'heure de la sortie doivent être au plus tôt demain');
            } elseif ($sortie->getDateLimiteInscription()->format('d m Y')
                >= $sortie->getDateHeureDebut()->format('d m Y')) {
                $this->addFlash('warning',
                    'La date limite d\'inscription doit être avant le jour de la sortie');
            } elseif ($sortie->getNbInscriptionsMax() < 1) {
                $this->addFlash('warning', 'Le nombre de participants doit être strictement positif');
            } elseif ($sortie->getDuree()->format('H:i') === '00:00') {
                $this->addFlash('warning', 'La durée doit être supérieure à 0 minute');
            } else {

    // Pour savoir quel bouton est cliqué, obligation d'instancier le formulaire comme étant un Form
    // et non un FormInterface pour accéder aux méthodes "getClickedButton" et "getName"
                if ($creationSortieForm instanceof Form) {
                    if ($creationSortieForm->getClickedButton()->getName() === 'enregistrer') {
                        $etat = $etatRepository->findOneByLibelle('Créée');
                        $sortie->setEtat($etat);
                        $this->addFlash("success", "Sortie enregistrée");
                    } elseif ($creationSortieForm->getClickedButton()->getName() === 'publier') {
                        $etat = $etatRepository->findOneByLibelle('Publiée');
                        $sortie->setEtat($etat);
                        $this->addFlash("success", "Sortie publiée");
                    }
                    $manager->persist($sortie);
                    $manager->flush();
                    return $this->redirectToRoute('sortie_home');
                }
            }
        }
        return $this->render('sorties/creation.html.twig', [
            'creationSortieForm' => $creationSortieForm->createView(),
            'errors' => $creationSortieForm->getErrors(true)
        ]);
    }

    /**
     * @Route("/listesDeroulantes/{id}", name="listesDeroulantes")
     */
    public function listesDeroulantes($id, LieuRepository $lieuRepository, VilleRepository $villeRepository)
    {
        $ville = $villeRepository->findOneById($id);
        $lieux = $lieuRepository->findByVille($ville);
        $data = (array_map(function ($lieux){
            return   ['label'=> $lieux->getNom(),
                'value'=> $lieux->getId(),
            ];
        },$lieux));

        return $this->json($data);
    }

    /**
     * @Route("/listeDeroulanteLieu/{id}", name="listeDeroulanteLieu")
     * @param $id
     * @param LieuRepository $lieuRepository
     * @return JsonResponse
     */
    public function listeDeroulanteLieu($id, LieuRepository $lieuRepository)
    {
        $lieu = $lieuRepository->findOneById($id);
        $tableauLieux[] = $lieu;
        $donnees = (array_map(function ($tableauLieux){
            return   [
                'rue' => $tableauLieux->getRue(),
                'latitude' => $tableauLieux->getLatitude(),
                'longitude' => $tableauLieux->getLongitude(),
                'codePostal' => $tableauLieux->getVille()->getCodePostal()
            ];
        },$tableauLieux));

        return $this->json($donnees);
    }

    /**
     * @Route("/detailAnnulerSortie/{id}", name="sortie_detailAnnuler")
     */
    public function detailAnnuler(int $id, SortieRepository $sortieRepository, Request $request){
        $sorties = new Sortie();
        $sorties = $sortieRepository->find($id);
        $motifAnnulationForm = $this->createForm(MotifAnnulationType::class, $sorties,
        ['action' => $this->generateUrl('sortie_annuler', ['id' => $id])]
        );


        return $this->render('sorties/sortie_detailAnnuler.html.twig', [
            'sortieChoisie' => $sorties,
            'motifAnnulation' => $motifAnnulationForm->createView(),
        ]);
    }


    /**
     * @Route("/rechercheSortie",name="rechercheSortie")
     */
    public function index(SortieRepository $repository, Request $request)
    {
        $data=new SearchData();
        $form=$this->createForm(SearchType::class,$data);
        $form->handleRequest($request);
        $sorties=$repository->findSearch($data);

        return $this->render('sorties/rechercheSortie.html.twig', ['sorties'=>$sorties,'form'=>$form->createView()
        ]);

    }

    //fonction annuler a travailler pour afficher une textarea du motif annulation
    //enregistrer le motif dans la bdd pour l'afficher dans la page detail de la sortie
    /**
     * @Route("/annuler/{id}", name="sortie_annuler")
     */
    public function annulerSortie(int $id, EntityManagerInterface $em, EtatRepository $etatRepository, Request $request){
        $sortieRepo = $em->getRepository(Sortie::class);
        $sortie = $sortieRepo->find($id);
        $organisateur = $sortie->getOrganisateur();
        $motifAnnulationForm = $this->createForm(MotifAnnulationType::class, $sortie);
        $motifAnnulationForm->handleRequest($request);

        if ($organisateur->getId() == $this->getUser()->getId() && $motifAnnulationForm->isSubmitted()) {
                $etat = $etatRepository->findOneByLibelle('Annulée');
                $sortie->setEtat($etat);
                $this->addFlash("success", "votre sortie a bien été annulée !");

        }else {
            $this->addFlash('warning', "Vous ne pouvez annuler la sortie que si vous en etes l'organisateur !");
        }
        $em->persist($sortie);
        $em->flush();

        return $this->redirectToRoute('sortie_home');
    }

    /**
     * @Route("/afficherUneSortie/{id}", name="sortie_afficher")
     */
    public function afficherUneSortie(int $id, SortieRepository $sortieRepository){
        $sorties = new Sortie();
        $sorties = $sortieRepository->find($id);
        return $this->render('sorties/sortie_afficher.html.twig', [
            'sortieChoisie' => $sorties,
        ]);
    }

}
