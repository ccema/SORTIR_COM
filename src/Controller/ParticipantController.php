<?php

namespace App\Controller;

use App\Entity\Inscription;
use App\Entity\Participant;
use App\Entity\Sortie;
use App\Form\ProfilType;
use App\Form\UpdatePasswordType;
use App\Repository\InscriptionRepository;
use App\Repository\ParticipantRepository;
use App\Repository\SortieRepository;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class ParticipantController extends AbstractController
{

    /**
     * @Route("/profil", name="profil")
     */
    public function profil(){
        return $this->render('profil/profil.html.twig', [
            'controller_name' => 'ParticipantController',
        ]);
    }

    /**
     * @Route("/profil/profil_update", name="profil_update")
     */
    public function update(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {

        //récupérer le profil de l'utilisateur connecter
        $user = $this->getUser();
        $motdepasseactuel = $this->getUser()->getPassword();
        //création formulaire maj
        $profilForm = $this->createForm(ProfilType::class, $user);
        $profilForm->handleRequest($request);
        //Vérification formulaire valide lors du submit
        if ($profilForm->isSubmitted() && $profilForm->isValid()){
            $motdepasse = $profilForm->get('motdepasse')->getData();
            //Vérification saisie mot de passe correcte
            if (password_verify($motdepasse, $motdepasseactuel)) {
                //encodage mot de passe
                $hashmotdepasse = $passwordEncoder->encodePassword($user, $motdepasse);
                $user->setMotdepasse($hashmotdepasse);
                //Envoi modification effectuer en BDD
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', 'Profil modifié !');
                //redirection vers le profil de l'utilisateur
                return $this->redirectToRoute('profil');
            }else{
                $this->addFlash('warning', 'Modification(s) non effectuée(s) (Mot de passe incorrect) !');
            }
        }

        return $this->render('profil/update.html.twig', [
            'profilForm' => $profilForm->createView()
        ]);
    }

    /**
     * @Route("/profil/password_update", name="password_update")
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        //Récupération mot de passe de la session
        $motdepasseactuel = $this->getUser()->getPassword();
        //création du formulaire
        $updatePassForm= $this->createForm(UpdatePasswordType::class, $user);
        $updatePassForm->handleRequest($request);
        //Vérification envoi du formulaire
        if ($updatePassForm->isSubmitted() && $updatePassForm->isValid()){
            $ancienmotdepasse = $user->getAncienmotdepasse();
            //verification saisie mot de passe égal mot de passe actuel
            if (password_verify($ancienmotdepasse, $motdepasseactuel)) {
                //récupération nouveau mot de passe saisie
                $newmotdepasse = $updatePassForm->get('motdepasse')['first']->getData();
                //encodage nouveau mot de passe
                $hashnewmotdepasse = $passwordEncoder->encodePassword($user, $newmotdepasse);
                //modifier mot de passe en BDD
                $user->setMotdepasse($hashnewmotdepasse);
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', 'Mot de passe modifié !');
                return $this->redirectToRoute('profil');
            }else{
                $this->addFlash('warning', 'Modification(s) non effectuée(s) (Mot de passe incorrect) !');
            }


        }
        //afficher le profil de l'utilisateur
        return $this->render('profil/updatePassword.html.twig', [
            'updatePassForm' => $updatePassForm->createView()
        ]);
    }

    /**
     * @Route("/detailInscriptionSortie/{id}", name="detailInscriptionSortie")
     */
    public function detailInscriptionSortie($id,EntityManagerInterface $em, SortieRepository $repository,Request $request){

        $sortie = $repository->findOneBy(['id'=>$id]);
        $dateCloture = $sortie->getDateLimiteInscription();
        $datedujour = new \DateTime();
        $maxInscrits = $sortie->getNbInscriptionsMax();
        $nbInscrits = $sortie->getInscriptions()->count();
        dump($nbInscrits);
        //si inscription cloturée ou nb d'inscrits max atteint:
        //inscription impossible
        if (($datedujour>$dateCloture) || ($nbInscrits>=$maxInscrits))
        {
            $this->addFlash('warning','Les inscriptions sont clôturées!!');
            return $this->redirectToRoute('sortie_home');
        }
        return $this->render('profil/detailInscriptionSortie.html.twig', [
            'sortieChoisie'=>$sortie
        ]);
    }
    /**
     * @Route("/inscriptionSortie/{id}", name="inscriptionSortie")
     */
    public function InscriptionSortie($id, EntityManagerInterface $em,SortieRepository $repository )
    {
        $participant=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $sortie = $repository->findOneBy(['id'=>$id]);
        $dateCloture = $sortie->getDateLimiteInscription();
        $datedujour = new \DateTime();
        $maxInscrits = $sortie->getNbInscriptionsMax();
        $nbInscrits = $sortie->getInscriptions()->count();
        //si inscription cloturée ou nb d'inscrits max atteint:
        //inscription impossible
        if (($datedujour>$dateCloture) || ($nbInscrits>=$maxInscrits))
        {
            $this->addFlash('warning','Les inscriptions sont clôturées!!');
            return $this->redirectToRoute('sortie_home');
        }else{
        $inscription = new Inscription();
        $inscription->setSortie($sortie);
        $inscription->setParticipant($participant);
        $em->persist($inscription);
        $em->flush();

        $this->addFlash('success', 'Vous êtes bien inscrit !');

        }
        return $this->redirectToRoute('sortie_home');

    }
    /**
     * @Route("/detailDesisterSortie/{id}", name="detailDesisterSortie")
     */
    public function detailDesisterSortie($id, EntityManagerInterface $em, SortieRepository $repository, Request $request)
    {

        $sortie = $repository->findOneBy(['id' => $id]);
        return $this->render('profil/detailDesisterSortie.html.twig', [
            'sortieChoisie' => $sortie
        ]);
    }
    /**
     * @Route("/desisterSortie/{id}", name="desisterSortie")
     */
    public function desisterSortie($id, EntityManagerInterface $em, InscriptionRepository $repository)
    {
        $participant = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $inscription = $repository->findOneBy(['sortie' => $id,'participant'=>$participant]);

        $em->remove($inscription);

        $em->flush();

        $this->addFlash('success', 'Vous êtes bien désinscrit !');


        return $this->redirectToRoute('sortie_home');

    }
    /**
     * @Route("/afficherSortie/{id}", name="afficherSortie")
     */
    public function afficher($id, EntityManagerInterface $em, SortieRepository $repository, Request $request)
    {

        $sortie = $repository->findOneBy(['id' => $id]);
        return $this->render('profil/afficherSortie.html.twig', [
            'sortieChoisie' => $sortie
        ]);
    }

    /**
     * @Route("/autreProfil/{id}", name="participant_autreProfil")
     */
    public function autreProfil($id, ParticipantRepository $repository){

        $autreParticipant = $repository->findOneBy(['id' => $id]);
        dump($repository);
        return $this-> render('profil/participant_autreProfil.html.twig', [
            'autreProfil' => $autreParticipant
        ]);

    }
}
