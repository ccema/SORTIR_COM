<?php

namespace App\Entity;

use App\Repository\EtatRepository;
use App\Repository\SortieRepository;
use DateInterval;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Validator\Constraints\Time;

/**
 * @ORM\Entity(repositoryClass=SortieRepository::class)
 */
class Sortie
{
    /*
     * ATTRIBUTS
     */

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private $nom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateHeureDebut;

    /**
     * @ORM\Column(type="time")
     */
    private $duree;

    /**
     * @ORM\Column(type="date")
     */
    private $dateLimiteInscription;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbInscriptionsMax;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $infosSortie;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $motifAnnulationSortie;

    /*
     * RELATIONS
     */

    /**
     * @ORM\ManyToOne(targetEntity=Site::class, inversedBy="sorties", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $site;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class, inversedBy="sorties", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $lieu;

    /**
     * @ORM\ManyToOne(targetEntity=Etat::class, inversedBy="sorties", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $etat;

    /**
     * @ORM\OneToMany(targetEntity=Inscription::class, mappedBy="sortie", orphanRemoval=true)
     */
    private $inscriptions;

    /**
     * @ORM\ManyToOne(targetEntity=Participant::class, inversedBy="sorties")
     * @ORM\JoinColumn(nullable=false)
     */
    private $organisateur;

    public function __construct()
    {
        $this->dateHeureDebut = new DateTime();
        $this->dateHeureDebut->add(new DateInterval('P1D'));
        $this->dateLimiteInscription = new DateTime();
        $this->dateLimiteInscription->setTime(0,0);
        $this->inscriptions = new ArrayCollection();
    }

    /*
     * GETTERS ET SETTERS ATTRIBUTS
     */

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateHeureDebut(): ?DateTimeInterface
    {
        return $this->dateHeureDebut;
    }

    public function setDateHeureDebut(DateTimeInterface $dateHeureDebut): self
    {
        $this->dateHeureDebut = $dateHeureDebut;

        return $this;
    }

    public function getDuree(): ?DateTimeInterface
    {
        return $this->duree;
    }

    public function setDuree(DateTimeInterface $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getDateLimiteInscription(): ?DateTimeInterface
    {
        return $this->dateLimiteInscription;
    }

    public function setDateLimiteInscription(DateTimeInterface $dateLimiteInscription): self
    {
        $this->dateLimiteInscription = $dateLimiteInscription;

        return $this;
    }

    public function getNbInscriptionsMax(): ?int
    {
        return $this->nbInscriptionsMax;
    }

    public function setNbInscriptionsMax(int $nbInscriptionsMax): self
    {
        $this->nbInscriptionsMax = $nbInscriptionsMax;

        return $this;
    }

    public function getInfosSortie(): ?string
    {
        return $this->infosSortie;
    }

    public function setInfosSortie(string $infosSortie): self
    {
        $this->infosSortie = $infosSortie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMotifAnnulationSortie()
    {
        return $this->motifAnnulationSortie;
    }

    /**
     * @param mixed $motifAnnulationSortie
     * @return Sortie
     */
    public function setMotifAnnulationSortie($motifAnnulationSortie)
    {
        $this->motifAnnulationSortie = $motifAnnulationSortie;
        return $this;
    }



    /*
     * GETTERS ET SETTERS AUTRES ENTITES
     */

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getLieu(): ?Lieu
    {
        return $this->lieu;
    }

    public function setLieu(?Lieu $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getEtat(): ?Etat
    {
        return $this->etat;
    }

    public function setEtat(?Etat $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->setSortie($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->contains($inscription)) {
            $this->inscriptions->removeElement($inscription);
            // set the owning side to null (unless already changed)
            if ($inscription->getSortie() === $this) {
                $inscription->setSortie(null);
            }
        }

        return $this;
    }

    public function getOrganisateur(): ?Participant
    {
        return $this->organisateur;
    }

    public function setOrganisateur(?Participant $organisateur): self
    {
        $this->organisateur = $organisateur;

        return $this;
    }

    /*
     * AUTRES
     */

    public function inscritSortie(int $id){
        $inscrit = $this ->getInscriptions();
        $verif = false;
        foreach ($inscrit as $i){
            if  ($i->getParticipant()->getId() === $id){
                $verif = true;
                break;
            }
        }

        return  $verif;
    }

    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * fonction servant à afficher le bon libellé de la sortie, selon son état en base,
     * et selon des critères de date et heure
     * ATTENTION @TODO méthode à réécrire, je n'ai fais que recopier le code provenant de la création de sortie, il s'agira de retourner un libellé
     * @param Sortie $sortie
     * @param EtatRepository $etatRepository
     * @throws \Exception
     */
    public function libelleSortie(Sortie $sortie, EtatRepository $etatRepository) {
        $dateJour = new DateTime();

        $dateHeureDebut = new DateTime();
        $dateHeureDebut->setTimestamp($sortie->getDateHeureDebut()->getTimestamp());

        $dateLimiteInscription = new DateTime();
        $dateLimiteInscription->setTimestamp($sortie->getDateLimiteInscription()->getTimestamp());

        $duree = $sortie->getDuree()->format('H:i');
        list($hours, $minutes) = sscanf($duree, '%d:%d');
        $interval = new DateInterval(sprintf('PT%dH%dM', $hours, $minutes));

        // si la date limite d'inscription est égal à aujourd'hui
        if ($dateLimiteInscription->format('d m Y') === $dateJour->format('d m Y')) {
            $etat = $etatRepository->findOneByLibelle('En cours');
            $sortie->setEtat($etat);
            // SINON SI la date limite d'inscription est AVANT aujourd'hui
        } elseif ($dateLimiteInscription->format('d m Y') < $dateJour->format('d m Y')) {
            $etat = $etatRepository->findOneByLibelle('Ouvert');
            $sortie->setEtat($etat);
            // DERNIER CAS : la sortie a débuté avant la date / heure du jour,
            // et sa fin était avant la date / heure du jour
            // (attention, à partir du else, $dateHeureDebut a pris la durée dans les dents)
        } elseif ($sortie->getDateHeureDebut() < new DateTime() && $dateHeureDebut->add($interval) < new DateTime()) {
            $etat = $etatRepository->findOneByLibelle('Fermé');
            $sortie->setEtat($etat);
            $sortie->getEtat()->setLibelle('Sortie finie');
        }
    }

}
