class LinkSelect{
    // le constructeur est la première chose exécutée
    constructor($select) {
        // création du lien entre les deux select
        this.$select = $select
        // fonction onChange : une nouvelle fonction est liée à la fonction onChange. Elle prend en paramètre this.
        // L'appel de la fonction liée entraîne l'exécution de sa fonction enveloppée
        // fonction nécessaire au bon déroulement du chargement de la liste déroulante, ainsi qu'au this.$select.dataset
        this.onChange = this.onChange.bind(this)
        // à chaque changement sur la liste déroulante, appel la fonction onChange
        this.$select.addEventListener('change', this.onChange)
    }
    onChange(e){
        // la requête est un nouvel élément de requête
        let request = new XMLHttpRequest()
        // c'est cette ligne là qui est importante : à chaque changement sur le select, elle va appeler une URL, notre path en l'occurrence, pour aller récupérer les données d'une fonction
        // c'est la fonction située au path donné qui va faire la recherche dans la BDD, et qui fournira les réponses à la requête
        request.open('GET',
            this.$select.dataset.source // l'URL
                // dataset pour accéder aux données de type data : data-url, data-source...
                // source correspond au path, qu'on a indiqué dans le formulaire, au niveau de la ville
                .replace('$id', e.target.value),
            // target contient le select, target.value pour accéder à la valeur de l'option qui a été cliquée
            true)
        // lorsque la requête est entièrement chargée, exécution d'une fonction
        request.onload = () => {
            // si le statut de la requête est entre 200 et 400
            if (request.status >=200 && request.status<400){
                // transforme le résultat de la requête en data (un objet)
                // la requête response (et donc le data, à terme) contiennent un tableau clé valeur
                // contenant un objet avec deux attributs : label et value
                let data = JSON.parse(request.responseText)
                // la fonction reduce est une fonction pour tous les éléments d'un tableau
                // la valeur retournée est une accumulation de résultat
                // c'est un accumulateur : elle accumule chaque élément, de la gauche vers la droite, en un seul élément
                // premier paramètre : l'accumulateur, second paramètre : la valeur actuelle
                let options = data.reduce(function (acc, option){
                    // on peut utiliser option.value (alors que value est un attribut de l'objet contenu par data)
                    // parce que option est la valeur actuelle du tableau d'objet
                    return acc + '<option value="' + option.value + '">' + option.label + '</option>'
                }, '')

                // on récupère la cible qui va recevoir les options : la seconde liste déroulante
                let $target = document.getElementById("creation_sortie_lieu")
                // la cible va récupérer les options créées en une seule ligne, format HTML
                // innerHTML : récupère ou définit la syntaxe HTML du descendant de l'élément
                $target.innerHTML = options
                $target.style.display = null
                let event = new Event('change')
                $target.dispatchEvent(event)
               //$target.onchange()

                let $rue = document.getElementById('creation_sortie_lieuForm_rue');
                $rue.setAttribute('value', '')
                $rue.removeAttribute('disabled')

                let $latitude = document.getElementById('creation_sortie_lieuForm_latitude');
                $latitude.setAttribute('value', '')
                $latitude.removeAttribute('disabled')

                let $longitude = document.getElementById('creation_sortie_lieuForm_longitude');
                $longitude.setAttribute('value', '')
                $longitude.removeAttribute('disabled')

                let $codePostal = document.getElementById('creation_sortie_lieuForm_ville_codePostal');
                $codePostal.setAttribute('value', '')
                $codePostal.removeAttribute('disabled')
            }
        }
        request.onerror = function (){
            alert('Impossible de charger la liste')
        }
        request.send()

    }
}

class LinkSelectLieu{
    constructor($selectLieu) {
        this.$selectLieu = $selectLieu
        this.auChangement = this.auChangement.bind(this)
        this.$selectLieu.addEventListener('change', this.auChangement)
    }
    auChangement(e) {
        let requete = new XMLHttpRequest()
        requete.open(
            'GET',
            this.$selectLieu.dataset.source.replace('$id', e.target.value),
            true
        )
        requete.onload = () => {
            if (requete.status >= 200 && requete.status < 400){
                let donnees = JSON.parse(requete.responseText)
                let valueRue = donnees.reduce(function (accumulateur, value){
                    return accumulateur + value.rue
                }, '')
                let $rue = document.getElementById('creation_sortie_lieuForm_rue');
                $rue.setAttribute('value', valueRue)
                $rue.setAttribute('disabled', 'disabled')

                let valueLatitude = donnees.reduce(function (accumulateur, value){
                    return accumulateur + value.latitude
                }, '')
                let $latitude = document.getElementById('creation_sortie_lieuForm_latitude');
                $latitude.setAttribute('value', valueLatitude)
                $latitude.setAttribute('disabled', 'disabled')

                let valueLongitude = donnees.reduce(function (accumulateur, value){
                    return accumulateur + value.longitude
                }, '')
                let $longitude = document.getElementById('creation_sortie_lieuForm_longitude');
                $longitude.setAttribute('value', valueLongitude)
                $longitude.setAttribute('disabled', 'disabled')

                let valueCodePostal = donnees.reduce(function (accumulateur, value){
                    return accumulateur + value.codePostal
                }, '')
                let $codePostal = document.getElementById('creation_sortie_lieuForm_ville_codePostal');
                $codePostal.setAttribute('value', valueCodePostal)
                $codePostal.setAttribute('disabled', 'disabled')
            }
        }
        requete.onerror = function (){
            alert('Impossible de charger les autres attributs')
        }
        requete.send()
    }
}

// récupération de l'élément sélectionné, première chose faite du JS
// il s'agit de tout le contenu du select
let $select = document.getElementById("creation_sortie_ville")
// appel de la class LinkSelect, avec en paramètre le select
new LinkSelect($select)

let $selectLieu = document.getElementById("creation_sortie_lieu");
new LinkSelectLieu($selectLieu)

let event = new Event('change')
$selectLieu.dispatchEvent(event)